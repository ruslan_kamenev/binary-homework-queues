<?php

namespace App\Jobs;

use App\Actions\Image\ApplyFilterAction;
use App\Values\Image;
use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private ApplyFilterAction $applyFilter,
        private Image $image,
        private array $request
    ) {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $this->applyFilter->execute($this->image, $this->filter);
        //ImageProcessedNotification

        throw new \Exception('Job is failed');
    }

    public function failed(\Exception $exception)
    {
        //ImageProcessingFailedNotification
    }
}
